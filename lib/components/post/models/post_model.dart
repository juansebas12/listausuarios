class PostModel {
  late List<Post> results;
  PostModel({required this.results});
  PostModel.fromJson(json) {
    if (json != null) {
      results = <Post>[];
      json.forEach((v) {
        results.add(Post.fromJson(v));
      });
    }
  }
}

class Post {
  Post({
    required this.userId,
    required this.id,
    required this.title,
    required this.body,
  });

  int userId;
  int id;
  String title;
  String body;

  factory Post.fromJson(Map<String, dynamic> json) => Post(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        body: json["body"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "id": id,
        "title": title,
        "body": body,
      };
}
