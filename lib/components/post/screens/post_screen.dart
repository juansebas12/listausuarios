import 'package:flutter/material.dart';
import 'package:flutter_lista_usurios/components/home/widgets/card_user_widget.dart';
import 'package:flutter_lista_usurios/components/post/blocs/post_bloc.dart';
import 'package:flutter_lista_usurios/components/post/models/post_model.dart';
import 'package:flutter_lista_usurios/components/post/widgets/card_post.dart';
import 'package:flutter_lista_usurios/components/users/models/user_model.dart';

class PostScreen extends StatelessWidget {
  final User user;
  PostScreen({Key? key, required this.user}) : super(key: key) {
    postsBloc.loadPost(user.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title:
            const Text('Publicaciones', style: TextStyle(color: Colors.white)),
      ),
      body: Column(
        children: [
          CardUserWidget(
            user: user,
            viewButton: false,
          ),
          const Center(
            child: Text('Publicaciones',
                style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                    fontSize: 24.0)),
          ),
          Expanded(child: _list(context)),
        ],
      ),
    );
  }

  Widget _list(BuildContext context) {
    return StreamBuilder<List<Post>>(
        stream: postsBloc.postsStream,
        builder: (context, snapshot) {
          return (!snapshot.hasData || snapshot.data!.isEmpty)
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: snapshot.data?.length,
                  itemBuilder: (BuildContext context, int index) {
                    return CardPostWidget(
                      post: snapshot.data![index],
                    );
                  });
        });
  }
}
