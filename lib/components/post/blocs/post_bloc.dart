import 'dart:async';

import 'package:flutter_lista_usurios/components/post/models/post_model.dart';
import 'package:flutter_lista_usurios/components/users/providers/user_provider.dart';
import 'package:rxdart/rxdart.dart';

class PostsBloc {
  BehaviorSubject<List<Post>> postController = BehaviorSubject<List<Post>>();
  late List<Post> posts;
  Stream<List<Post>> get postsStream => postController.stream;

  UserProvider userProvider = UserProvider();

  loadPost(int idUser) async {
    postController.sink.add(<Post>[]);
    var resultsApi = await userProvider.loadPostByUserId(idUser);
    posts = PostModel.fromJson(resultsApi).results;
    postController.sink.add(posts);
  }
}

final postsBloc = PostsBloc();
