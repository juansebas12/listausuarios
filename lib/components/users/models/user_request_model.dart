import 'package:flutter_lista_usurios/components/users/models/user_model.dart';

class UserRequestModel {
  late List<User> results;
  UserRequestModel({required this.results});
  UserRequestModel.fromJson(json) {
    if (json != null) {
      results = <User>[];
      json.forEach((v) {
        results.add(User.fromJson(v));
      });
    }
  }
}
