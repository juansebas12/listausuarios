import 'dart:convert';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;

class UserProvider {
  loadUsersProvider() async {
    Response httpResponse =
        await http.get(Uri.parse("https://jsonplaceholder.typicode.com/users"));
    if (httpResponse.statusCode == 200) {
      final response = json.decode(utf8.decode(httpResponse.bodyBytes));
      return response;
    }
    return null;
  }

  loadPostByUserId(int idUser) async {
    Response httpResponse = await http.get(
        Uri.parse("https://jsonplaceholder.typicode.com/posts?userId=$idUser"));
    if (httpResponse.statusCode == 200) {
      final response = json.decode(utf8.decode(httpResponse.bodyBytes));
      return response;
    }
    return null;
  }
}
