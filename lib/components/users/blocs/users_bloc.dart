import 'dart:async';

import 'package:flutter_lista_usurios/components/users/models/user_model.dart';
import 'package:flutter_lista_usurios/components/users/models/user_request_model.dart';
import 'package:flutter_lista_usurios/components/users/providers/user_provider.dart';
import 'package:flutter_lista_usurios/db/databaseApp.dart';
import 'package:rxdart/rxdart.dart';

class UsersBloc {
  BehaviorSubject<List<User>> usersController = BehaviorSubject<List<User>>();
  late List<User> userRequestModel;
  late List<User> userRequestModelSearch;
  Stream<List<User>> get usersStream => usersController.stream;

  UserProvider userProvider = UserProvider();

  loadUsers() async {
    var resultsdb = await DatabaseApp().loadUsers();
    if (resultsdb.isEmpty) {
      var resultsApi = await userProvider.loadUsersProvider();
      userRequestModel = UserRequestModel.fromJson(resultsApi).results;
      usersController.sink.add(userRequestModel);
      if (userRequestModel.isNotEmpty) {
        await DatabaseApp().insertUsers(userRequestModel);
      }
    } else {
      userRequestModel = UserRequestModel.fromJson(resultsdb).results;
      usersController.sink.add(userRequestModel);
    }
  }

  searchUser(String value) {
    if (value == "") {
      usersController.sink.add(userRequestModel);
    } else {
      userRequestModelSearch = <User>[];
      for (var i = 0; i < userRequestModel.length; i++) {
        User user = userRequestModel[i];
        if (user.name.toLowerCase().contains(value.toLowerCase())) {
          userRequestModelSearch.add(user);
        }
      }
      usersController.sink.add(userRequestModelSearch);
    }
  }
}

final usersBloc = UsersBloc();
