import 'package:flutter/material.dart';
import 'package:flutter_lista_usurios/components/home/widgets/card_user_widget.dart';
import 'package:flutter_lista_usurios/components/home/widgets/search_widget.dart';
import 'package:flutter_lista_usurios/components/users/blocs/users_bloc.dart';
import 'package:flutter_lista_usurios/components/users/models/user_model.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({
    Key? key,
  }) : super(key: key) {
    usersBloc.loadUsers();
  }

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Text('Prueba de ingreso',
            style: TextStyle(color: Colors.white)),
      ),
      body: Column(
        children: [
          const SearchWidget(),
          Expanded(child: _list(context)),
        ],
      ),
    );
  }

  Widget _list(BuildContext context) {
    return StreamBuilder<List<User>>(
        stream: usersBloc.usersStream,
        builder: (context, snapshot) {
          return !snapshot.hasData
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: snapshot.data?.length,
                  itemBuilder: (BuildContext context, int index) {
                    return CardUserWidget(
                      user: snapshot.data![index],
                      viewButton: true,
                    );
                  });
        });
  }
}
