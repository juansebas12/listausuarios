import 'package:flutter/material.dart';
import 'package:flutter_lista_usurios/components/users/blocs/users_bloc.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: TextField(
          onChanged: (value) {
            usersBloc.searchUser(value);
          },
          style: const TextStyle(
            color: Colors.green,
          ),
          decoration: const InputDecoration(
            hoverColor: Colors.green,
            contentPadding: EdgeInsets.all(10.0),
            labelText: 'Buscar usuario',
            labelStyle: TextStyle(
              color: Colors.green,
            ),
          ),
        ));
  }
}
