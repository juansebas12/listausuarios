import 'package:flutter/material.dart';
import 'package:flutter_lista_usurios/components/post/screens/post_screen.dart';
import 'package:flutter_lista_usurios/components/users/models/user_model.dart';

class CardUserWidget extends StatelessWidget {
  final User user;
  final bool viewButton;
  const CardUserWidget({Key? key, required this.user, required this.viewButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
      padding: const EdgeInsets.all(8.0),
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              blurRadius: 7,
              offset: const Offset(0, 3),
            ),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(user.name,
              style: const TextStyle(
                  color: Colors.green,
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0)),
          Row(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const Padding(
                padding: EdgeInsets.only(right: 4.0),
                child: Icon(Icons.phone, color: Colors.green),
              ),
              Text(user.phone),
            ],
          ),
          Row(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const Padding(
                padding: EdgeInsets.only(right: 4.0),
                child: Icon(Icons.email, color: Colors.green),
              ),
              Text(user.email),
            ],
          ),
          viewButton
              ? Container(
                  alignment: Alignment.centerRight,
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (ctx) => PostScreen(
                                    user: user,
                                  )));
                    },
                    child: const Text('VER PUBLICACIONES',
                        style: TextStyle(
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                            fontSize: 12.0)),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
