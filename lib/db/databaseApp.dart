// ignore_for_file: file_names

import 'package:flutter_lista_usurios/components/users/models/address_model.dart';
import 'package:flutter_lista_usurios/components/users/models/company_model.dart';
import 'package:flutter_lista_usurios/components/users/models/user_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseApp {
  Future<Database> openDb() async {
    return openDatabase(
      join(await getDatabasesPath(), 'database.db'),
      onCreate: (db, version) => {
        db.execute("""CREATE TABLE IF NOT EXISTS user (
          id INTEGER,
          name varchar(255),
          username varchar(255),
          email varchar(255),
          phone varchar(255),
          website varchar(255))
        """),
        db.execute("""CREATE TABLE IF NOT EXISTS address (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_user INTEGER NOT NULL,
          street varchar(255),
          suite varchar(255),
          city varchar(255),
          zipcode varchar(255),
          FOREIGN KEY(id_user) REFERENCES user(id))
        """),
        db.execute("""CREATE TABLE IF NOT EXISTS geo (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_address INTEGER,
          lat varchar(255),
          lng varchar(255),
          FOREIGN KEY(id_address) REFERENCES address(id))
        """),
        db.execute("""CREATE TABLE IF NOT EXISTS company (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_user INTEGER,
          name varchar(255),
          catchPhrase varchar(255),
          bs varchar(255),
          FOREIGN KEY(id_user) REFERENCES user(id))
        """),
      },
      onOpen: (db) => {},
      version: 1,
    );
  }

  Future<void> insertUsers(List<User> users) async {
    Database db = await openDb();
    await db.execute("DELETE FROM user");
    await db.execute("DELETE FROM address");
    await db.execute("DELETE FROM geo");
    await db.execute("DELETE FROM company");
    for (var user in users) {
      int response = await db.insert("user", {
        'id': user.id,
        'name': user.name,
        'username': user.username,
        'email': user.email,
        'phone': user.phone,
        'website': user.website
      });
      insertAddress(user.address, response);
      insertCompany(user.company, response);
    }
  }

  Future<void> insertAddress(Address address, int idUser) async {
    Database db = await openDb();
    int response = await db.insert("address", {
      'id_user': idUser,
      'street': address.street,
      'suite': address.suite,
      'city': address.city,
      'zipcode': address.zipcode
    });
    insertGeo(address.geo, response);
  }

  Future<void> insertGeo(Geo geo, int idAddress) async {
    Database db = await openDb();
    await db.insert(
        "geo", {'id_address': idAddress, 'lat': geo.lat, 'lng': geo.lng});
  }

  Future<void> insertCompany(Company company, int idUser) async {
    Database db = await openDb();
    await db.insert("company", {
      'id_user': idUser,
      'name': company.name,
      'catchPhrase': company.catchPhrase,
      'bs': company.bs
    });
  }

  Future<List<Map<String, dynamic>>> loadUsers() async {
    Database db = await openDb();
    List<Map<String, dynamic>> users = <Map<String, dynamic>>[];
    List<Map> data = await db.query('user');
    for (var i = 0; i < data.length; i++) {
      Map<String, dynamic> user = Map<String, dynamic>.from(data[i]);
      user['address'] = await loadAddress(user['id']);
      user['company'] = await loadCompany(user['id']);
      users.add(user);
    }
    return users;
  }

  Future<Map<String, dynamic>> loadAddress(int idUser) async {
    Database db = await openDb();
    List<Map> data = await db.query('address',
        where: "id_user = ?", whereArgs: [idUser], limit: 1);
    Map<String, dynamic> address = Map<String, dynamic>.from(data[0]);
    address['geo'] = await loadGeo(address['id']);
    return address;
  }

  Future<Map<String, dynamic>> loadGeo(int idAddress) async {
    Database db = await openDb();
    List<Map> data = await db.query('geo',
        where: "id_address = ?", whereArgs: [idAddress], limit: 1);
    Map<String, dynamic> geo = Map<String, dynamic>.from(data[0]);
    return geo;
  }

  Future<Map<String, dynamic>> loadCompany(int idUser) async {
    Database db = await openDb();
    List<Map> data = await db.query('company',
        where: "id_user = ?", whereArgs: [idUser], limit: 1);
    Map<String, dynamic> company = Map<String, dynamic>.from(data[0]);
    return company;
  }
}
